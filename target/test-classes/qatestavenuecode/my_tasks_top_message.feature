# Assumption: the user is logged on the system.

Feature: Top message on 'My Tasks' greets the user
  Whenever the user enters 'My Tasks', a message should indicate their tasks are listed below.

  Scenario: User accesses 'My Tasks'.

    Given The user is on the home page
    When They access My Tasks
    Then My Tasks should display "Hey Priscila, this is your todo list for today:"

