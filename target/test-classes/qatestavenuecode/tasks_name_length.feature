Feature: Task has a minimum and maximum length
  Tasks can't be too short, empty, nor longer than 250 characters.

  Scenario: User creates a task with an empty name
    Given An empty name for a task
    When The user adds the task
    Then The empty task is not created

  Scenario: User inserts a task name longer than 250 characters
    Given A task called "TodayIshouldgotothegymwashthedishestakemydogtothevetalsoIreallyneedtogotothelibrarytheycalledyesterdayandthebookIwantedsomuchhasarrivedafterthatIguessIlljustwatchafilmIhaventwatchedthenewIpManfilmanditseemsgreatmanIlovekungfusomuchohandthenIllhavedinner"
    When The user adds the task
    Then The task is not created

