Feature: Task description available on 'Manage Subtasks' as read only
  On the 'Manage Subtasks' dialog, the task description is shown, but as a read only feature

  Scenario: User accesses 'Manage Subtasks'

    Given The user is on the home page
    When They access My Tasks
    And Manage Subtasks dialog is accessed
    Then The task description is displayed as a read only feature