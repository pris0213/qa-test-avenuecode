package qatestavenuecode;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

public class ToDoAppStepdefs {
    private final WebDriver driver = new FirefoxDriver();
    private boolean loggedIn = false;

    @Before
    private void signIn() {
        accessToDoApp();

        driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[2]/ul[2]/li[1]/a")).click();
        String username = "giovpriscila@gmail.com";
        driver.findElement(By.id("user_email")).sendKeys(username);
        String password = "ppaodot2019";
        driver.findElement(By.id("user_password")).sendKeys(password);
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/form/input")).click();

        loggedIn = true;
    }

    private void accessToDoApp() {
        driver.get("https://qa-test.avenuecode.com/");
    }

    /*
     * Feature: my_tasks_top_message.feature
     */

    @Given("^The user is on the home page$")
    public void goToHomePage() {
        signIn();
        // Access Home option in nav bar
        driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[2]/ul[1]/li[1]/a")).click();
    }

    @When("^They access My Tasks$")
    public void goToMyTasks() {
        // Access My Tasks option in nav bar
        driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[2]/ul[1]/li[2]/a")).click();
    }


    @Then("^My Tasks should display \"([^\"]*)\"$")
    public void testTopMessageGreetsUser(String expectedMessage){
        // Get top message
        String actualMessage = driver.findElement((By.xpath("/html/body/div[1]/h1"))).getText();
        assertEquals(expectedMessage, actualMessage);

    }

    /*
     * Feature: tasks_name_length
     */

    @Given("^An empty name for a task$")
    public void insertEmptyTaskName() {
        signIn();
        goToMyTasks();
        driver.findElement(By.id("new_task")).sendKeys("");
    }

    @When("^The user adds the task$")
    public void addTask() {
        // Access add task button
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[2]/span")).click();
    }


    @Then("^The empty task is not created$")
    public void testEmptyTaskNotCreated() {
        String newTaskClass = driver
                .findElement(By
                        .xpath("/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[2]/a"))
                .getAttribute("class");
        // This is how the class attribute looks like when the task is empty
        assertNotEquals("ng-scope ng-binding editable editable-click editable-empty", newTaskClass);

    }

    @Given("^A task called \"([^\"]*)\"$")
    public void insertTaskWithMoreThan250Chars(String taskName) {
        signIn();
        goToMyTasks();
        driver.findElement(By.id("new_task")).sendKeys(taskName);
    }

    @Then("^The task is not created$")
    public void testTaskWithMoreThan250CharsNotCreated() {
        boolean isTaskCreated = false;
        List<WebElement> tasks = driver.findElements(By.xpath("/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr"));
        if (tasks.size() != 0) {
            isTaskCreated = true;
        }
        assertFalse(isTaskCreated);
    }

    /*
    * Feature: date_format
    * */

    @Given("^The date \"([^\"]*)\"$")
    public void insertDueDate(String dueDate) {
        signIn();
        goToMyTasks();
        accessManageSubtasksDialog();
        // Inserts subtask description, due date
        driver.findElement(By.id("new_sub_task")).sendKeys("New Subtask");
        driver.findElement(By.id("dueDate")).sendKeys(dueDate);
    }

    @When("^The button Add is selected$")
    public void addSubtask() {
        driver.findElement(By.id("add-subtask")).click();
    }

    @Then("^A new subtask can't be created$")
    public void testSubtaskWithInconsistentDateNotCreated() {
        // Subtasks are hidden until a subtask is created
        String subTaskList = driver.findElement(By.xpath("/html/body/div[4]/div/div/div[2]/div[2]")).getAttribute("class");
        boolean isHidden = false;
        if (!subTaskList.equals("ng-hide")) {
            isHidden = true;
        }
        assertFalse(isHidden);
        closeManageSubtasksDialog();

    }

    @And("^Manage Subtasks dialog is accessed$")
    public void accessManageSubtasksDialog() {
        addTask();
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr/td[4]/button")).click();

    }

    /*
    * Feature: manage_subtasks_task_description_is_read_only
    * */

    @Then("^The task description is displayed as a read only feature$")
    public void testTaskDescriptionIsReadOnly() {
        String attrReadOnly = driver.findElement(By.xpath("/html/body/div[4]/div/div/div[2]/form/textarea"))
                .getAttribute("readonly");
        assertNotNull(attrReadOnly);
        closeManageSubtasksDialog();
    }

    private void closeManageSubtasksDialog() {
        driver.findElement(By.xpath("/html/body/div[4]/div/div/div[3]/button")).click();
    }

    @After
    public void closeBrowser() {
        driver.quit();
    }
}