Feature: Date format should follow MM/dd/yyyy
  Date inputs should follow the pattern MM/dd/yyyy

  Scenario: User inserts date with non-existent month

    Given The date "14/02/2019"
    When The button Add is selected
    Then A new subtask can't be created