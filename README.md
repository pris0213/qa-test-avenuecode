# ToDo App - QA Test - AvenueCode

ToDo App is an application to manage tasks. Users can add/remove tasks and subtasks and set due dates to them.

## Goal
Testers have to complete the following criterea:

* Read the User Stories (US) provided.
* Perform manual testing based on the US and report bugs found during this proccess.
* Write automated tests using BDD.
* Write a PDF file about what other kinds of automated tests can be performed and how the value of testing could be maximized, plus other considerations that may seem relevant.

## User Stories
The US provided are:

1. __Create Task__: as a ToDo App user, I should be able to create a task so I can manage my tasks.
2. __Create SubTask__: as a ToDo App user, I should be able to create a subtask so I can  break down my tasks in smaller pieces.

## Implementation
Tests were developed in IntelliJ with Cucumber for Java.

### Requirements
* Java SE _(Java 9 and higher are not supported by Cucumber)_
* Maven _(version 3.3.1 or higher)_
* IntelliJ IDEA
	* IntelliJ IDEA Cucumber for Java plugin

### Run Tests
In IntelliJ terminal, type the following command to run the test set:

   
```shell

$ mvn test
```

Observation: tests don't pass, since they're based on bugs found on ToDo App.

### Project Structure
Features can be found under `/src/test/resources/qatestavenuecode`.

Tests can be found under `/src/test/java/qatestavenuecode`.

PDF file can be found in root folder, named `Considerations (Artifact 3).pdf`.

### Access to Bug Reports
To access my bug reports, use `giovpriscila@gmail.com` as username and `ppaodot2019` as password.





